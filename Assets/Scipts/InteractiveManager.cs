﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class InteractiveManager : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private Image handImage;
    [SerializeField]
    private LayerMask layerMask;
    [SerializeField]
    private float interactDistance;
    [SerializeField]
    private FlashLight flashLight;

    [SerializeField]
    HealthBar healthBar;
    [SerializeField]
    private Light candle;
    [SerializeField]
    private Image note;
    [SerializeField]
    private GameObject safeUi;

    [SerializeField]
    private RigidbodyFirstPersonController playerControl;

    [SerializeField]
    private GameObject key, door;
    private bool hasKey = false;
    void Start()
    {
        //PlayerPrefs.SetInt("Notes", 0);
        candle.gameObject.SetActive(false);
        handImage.gameObject.SetActive(false);
        note.gameObject.SetActive(false);
        safeUi.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //Cursor.visible = true;
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit raycastHit;
        if (Physics.Raycast(ray, out raycastHit, interactDistance, layerMask))
        {
            if (!handImage.gameObject.activeSelf)
            {
                handImage.gameObject.SetActive(true);
            }
            //Debug.Log("Ray hit object: " + raycastHit.transform.name);
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (raycastHit.transform.tag == "AidKit")
                {
                    healthBar.HealthBarAdd(100);
                    Destroy(raycastHit.transform.gameObject);
                    raycastHit.transform.gameObject.SetActive(false);
                }


                if (raycastHit.transform.tag == "Battery")
                {
                    flashLight.FlashLightEnergyAdd(100);
                    Destroy(raycastHit.transform.gameObject);
                }
                if (raycastHit.transform.tag == "Candle")
                {
                    if (candle.gameObject.activeSelf)
                    {
                        candle.gameObject.SetActive(false);
                    }
                    else candle.gameObject.SetActive(true);
                }

                if (raycastHit.transform.tag == "Key")
                {
                    Key key = raycastHit.transform.GetComponent<Key>();
                    if (key != null)
                    {
                        key.PickUp();
                    }
                    // Destroy(raycastHit.transform.gameObject);
                    // hasKey = true;
                }

                if (raycastHit.transform.tag == "Safe")
                {
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    safeUi.gameObject.SetActive(true);
                    playerControl.enabled = false;

                }

                if (raycastHit.transform.tag == "Door")
                {
                    Door doorState = raycastHit.transform.GetComponent<Door>();
                    if (doorState != null)
                    {
                        doorState.ChangeDoorState();
                    }
                }

                if (raycastHit.transform.tag == "DoorWithNote")
                {
                    DoorWithNote doorState2 = raycastHit.transform.GetComponent<DoorWithNote>();
                    if (doorState2 != null)
                    {

                        if (PlayerPrefs.GetInt("Notes") % 3 == 0 && PlayerPrefs.GetInt("Notes") != 0)
                        {
                            doorState2.Unlock();
                            doorState2.ChangeDoorState();

                        }
                        //doorState.ChangeDoorState();
                    }
                }

                if (raycastHit.transform.tag == "Note")
                {
                    note.gameObject.SetActive(true);
                    playerControl.enabled = false;

                    Note note2 = raycastHit.transform.GetComponent<Note>();
                    note2.PickUp();
                    Destroy(raycastHit.transform.gameObject);

                }
            }
        }
        else
        {
            handImage.gameObject.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Escape) && note.gameObject.activeSelf == true)
        {
            note.gameObject.SetActive(false);
            playerControl.enabled = true;
        }
        if (Input.GetKeyDown(KeyCode.Escape) && safeUi.gameObject.activeSelf == true)
        {
            Cursor.visible = false;
            safeUi.gameObject.SetActive(false);
            playerControl.enabled = true;
        }

    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Anomaly")
        {
            flashLight.gameObject.SetActive(false);
        }
        Debug.Log("Collision Hit" + other.gameObject.name);
    }
}
