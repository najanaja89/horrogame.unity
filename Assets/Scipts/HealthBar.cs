﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{

    [SerializeField]
    Image healthBar;
    [SerializeField]
    private int maxLife;
    private int curLife;
    void Start()
    {
        maxLife=PlayerPrefs.GetInt("HealthStatus");
        curLife = maxLife;
    }

    // Update is called once per frame
    void Update()
    {
        if (curLife > 0)
        {
            var lifeSize = new Vector3(curLife/maxLife, healthBar.transform.lossyScale.y, healthBar.transform.lossyScale.z);
            healthBar.transform.localScale=lifeSize;
        }

    }

    public void HealthBarAdd(int capacity)
    {
        curLife = capacity;
    }

    public int CurrentHealth()
    {
        return curLife;
    }
    public void HealthBarMinus(int capacity)
    {
        curLife -= capacity;
    }
    public void SetHealth(int health)
    {
        curLife = health;
    }
}
