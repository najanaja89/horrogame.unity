﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class SafeUI : MonoBehaviour
{

    [SerializeField]
    Safe safe;
    [SerializeField]
    Button one;
    [SerializeField]
    Button two;
    [SerializeField]
    Button three;
    [SerializeField]
    Button four;
    [SerializeField]
    Button five;
    [SerializeField]
    Button six;
    [SerializeField]
    Button check;

    [SerializeField]
    string password = "111111";

    [SerializeField]
    private RigidbodyFirstPersonController playerControl;


    void Start()
    {
        //safeOpenPlane.gameObject.SetActive(false);
        one.onClick.AddListener(delegate { ButtonClick(one); });
        two.onClick.AddListener(delegate { ButtonClick(two); });
        three.onClick.AddListener(delegate { ButtonClick(three); });
        four.onClick.AddListener(delegate { ButtonClick(four); });
        five.onClick.AddListener(delegate { ButtonClick(five); });
        six.onClick.AddListener(delegate { ButtonClick(six); });
        check.onClick.AddListener(ButtonCheck);
    }



    // Update is called once per frame
    void Update()
    {

    }

    void ButtonClick(Button button)
    {
        if (button.GetComponentInChildren<Text>().text == "0")
        {
            button.GetComponentInChildren<Text>().text = "1";
        }
        else if (button.GetComponentInChildren<Text>().text == "1")
        {
            button.GetComponentInChildren<Text>().text = "2";
        }
        else if (button.GetComponentInChildren<Text>().text == "2")
        {
            button.GetComponentInChildren<Text>().text = "3";
        }
        else if (button.GetComponentInChildren<Text>().text == "3")
        {
            button.GetComponentInChildren<Text>().text = "4";
        }
        else if (button.GetComponentInChildren<Text>().text == "4")
        {
            button.GetComponentInChildren<Text>().text = "5";
        }
        else if (button.GetComponentInChildren<Text>().text == "5")
        {
            button.GetComponentInChildren<Text>().text = "6";
        }
        else if (button.GetComponentInChildren<Text>().text == "6")
        {
            button.GetComponentInChildren<Text>().text = "7";
        }
        else if (button.GetComponentInChildren<Text>().text == "7")
        {
            button.GetComponentInChildren<Text>().text = "8";
        }
        else if (button.GetComponentInChildren<Text>().text == "8")
        {
            button.GetComponentInChildren<Text>().text = "9";
        }
        else if (button.GetComponentInChildren<Text>().text == "9")
        {
            button.GetComponentInChildren<Text>().text = "0";
        }
    }

    void ButtonCheck()
    {
        string combination = one.GetComponentInChildren<Text>().text +
        two.GetComponentInChildren<Text>().text +
        three.GetComponentInChildren<Text>().text +
        four.GetComponentInChildren<Text>().text +
        five.GetComponentInChildren<Text>().text +
        six.GetComponentInChildren<Text>().text;

        if (combination == password)
        {
            this.gameObject.SetActive(false);
            playerControl.enabled = true;
            safe.Unlock();
            Debug.Log("Safe Open");
        }
        else Debug.Log("Safe Closed");
    }
    private void OnDestroy()
    {
        one.onClick.RemoveAllListeners();
        two.onClick.RemoveAllListeners();
        three.onClick.RemoveAllListeners();
        four.onClick.RemoveAllListeners();
        five.onClick.RemoveAllListeners();
        six.onClick.RemoveAllListeners();
        check.onClick.RemoveAllListeners();
    }
}
