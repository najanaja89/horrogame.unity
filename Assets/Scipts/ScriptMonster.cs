﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEditor.Build;

public class ScriptMonster : MonoBehaviour
{
    [SerializeField]
    AudioSource audioScream;

    [SerializeField]
    HealthBar healthBar;

    [SerializeField]
    Transform player;
    [SerializeField]
    NavMeshAgent navMesh;
    [SerializeField]
    Animator animator;
    [SerializeField]
    Transform eyes;
    [Header("Camera Settings")]
    [SerializeField]
    Transform deathCamera;

    [SerializeField]
    Transform deathCameraPoint;

    [SerializeField]
    int healthBarStatus;

    string state = "idle";
    bool isAlive = true;
    bool highAlertness = false;
    float alertnessLevel = 20f;
    void Start()
    {
        healthBar.SetHealth(PlayerPrefs.GetInt("HealthStatus"));
        healthBarStatus = healthBar.CurrentHealth();
        navMesh.speed = 1f;
        animator.speed = 1f;
        deathCamera.gameObject.SetActive(false);
    }

    float waitTime = 2f;
    // Update is called once per frame
    void Update()
    {
        if (!isAlive)
        {

            return;
        }
        //передаем в аниматор скорость персонажа
        animator.SetFloat("velocity", navMesh.velocity.magnitude);
        if (state == "idle")
        {
            GotoRandomPoint();
        }
        if (state == "walk")
        {
            StopWalking();
        }
        if (state == "search")
        {
            SearchForPlayer();
        }

        if (state == "chase")
        {
            ChaseForPlayer();
        }
        if (state == "hunt")
        {
            HuntForPlayer();
        }

        if (state == "kill")
        {
            deathCamera.transform.position = Vector3.Slerp(deathCamera.position, deathCameraPoint.position, 30f * Time.deltaTime);
            deathCamera.transform.rotation = Quaternion.Slerp(deathCamera.rotation, deathCameraPoint.rotation, 30f * Time.deltaTime);
            animator.speed = 0.4f;
        }
        // двигаться к определнному объекту
        //navMesh.SetDestination(player.position);
    }


    void GotoRandomPoint()
    {
        Vector3 randomPosition = Random.insideUnitSphere * 20f;
        NavMeshHit navMeshHit;
        NavMesh.SamplePosition(transform.position + randomPosition, out navMeshHit, 20f, NavMesh.AllAreas);

        if (highAlertness)
        {
            NavMesh.SamplePosition(player.transform.position + randomPosition, out navMeshHit, 20f, NavMesh.AllAreas);
            alertnessLevel -= 5f;
            if (alertnessLevel <= 0)
            {
                highAlertness = false;
                navMesh.speed = 1f;
                animator.speed = 1f;
            }
        }



        navMesh.SetDestination(navMeshHit.position);
        state = "walk";
        Debug.Log("State: Going to next point");
    }

    public void CheckSight()
    {
        if (!isAlive) return;
        RaycastHit hit;
        if (Physics.Linecast(eyes.position, player.position, out hit))
        {
            if (hit.collider.tag == "Player")
            {
                if (state == "kill") return;
                state = "chase";


                //play auido
            }
        }
    }

    void ChaseForPlayer()
    {
        navMesh.speed = 2f;
        animator.speed = 2f;
        navMesh.SetDestination(player.position);
        float distance = Vector3.Distance(transform.position, player.transform.position);
        if (distance > 10f)
        {
            state = "hunt";
        }
        else if (navMesh.remainingDistance <= navMesh.stoppingDistance && !navMesh.pathPending)
        {
            Player playerController = player.GetComponent<Player>();
            if (playerController.isAlive)
            {
                state = "kill";
                KillPlayer();
            }
        }

    }
    void HuntForPlayer()
    {
        if (navMesh.remainingDistance <= navMesh.stoppingDistance && !navMesh.pathPending)
        {
            state = "search";
            waitTime = 5f;
            highAlertness = true;
            alertnessLevel = 20f;
            CheckSight();
        }
    }
    void SearchForPlayer()
    {
        if (waitTime > 0)
        {
            waitTime -= Time.deltaTime;
            transform.Rotate(0, 120f * Time.deltaTime, 0);
        }
        else
        {
            state = "idle";
        }
    }
    void StopWalking()
    {
        if (navMesh.remainingDistance <= navMesh.stoppingDistance && !navMesh.pathPending)
        {
            state = "search";
            waitTime = 5f;
            Debug.Log("State: Search");
        }
    }
    void KillPlayer()
    {
        animator.SetTrigger("Kill");
        audioScream.Play();
        player.GetComponent<Player>().isAlive = false;
        player.GetComponent<RigidbodyFirstPersonController>().enabled = false;
        deathCamera.gameObject.SetActive(true);

        deathCamera.position = Camera.main.transform.position;
        deathCamera.rotation = Camera.main.transform.rotation;
        Camera.main.gameObject.SetActive(false);

        if (healthBarStatus <= 0)
        {
            PlayerPrefs.SetInt("HealthStatus", 100);
            return;
        }
       else
        {
            healthBar.HealthBarMinus(26);
            healthBarStatus = healthBar.CurrentHealth();

            Invoke("RestartGame", 2f);
        }



    }

    void RestartGame()
    {
        PlayerPrefs.SetInt("HealthStatus", healthBarStatus);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
       
     
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, 20f);
    }

}
