﻿using UnityEngine;


public class Eyes : MonoBehaviour
{
    [SerializeField]
    private ScriptMonster monster;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            monster.CheckSight();
        }
    }
}
