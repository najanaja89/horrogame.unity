﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorWithNote : MonoBehaviour
{
    private bool isOpen;

    private bool IsLocked = true;
    private float doorOpenAngle = 90f;
    private float doorCloseAngle = 0f;
    private float smooth = 2f;

    // Start is called before the first frame update

    // Update is called once per frame

    private void Start()
    {
        PlayerPrefs.SetInt("Notes",0);
    }
    void Update()
    {
        if (IsLocked) return;
        if (isOpen)
        {
            Quaternion targetRotationOpen = Quaternion.Euler(0, doorOpenAngle, 0);
            transform.parent.localRotation = Quaternion.Slerp(transform.parent.localRotation, targetRotationOpen, smooth * Time.deltaTime);
        }
        else
        {
            Quaternion targetRotationClosed = Quaternion.Euler(0, doorCloseAngle, 0);
            transform.parent.localRotation = Quaternion.Slerp(transform.parent.localRotation, targetRotationClosed, smooth * Time.deltaTime);
        }

    }

    public void Unlock()
    {
        IsLocked = false;
    }
    public void ChangeDoorState()
    {
        isOpen = !isOpen;
    }
}
