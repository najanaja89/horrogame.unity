﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anomaly : MonoBehaviour
{
    [SerializeField]
    AudioSource audioScream;

    [SerializeField]

    private float timerAnomaly;

    [SerializeField]
    private FlashLight flashlight;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
     private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            audioScream.Play();
            flashlight.FlashLightEnergyMinus(25);
        }
    }
}
