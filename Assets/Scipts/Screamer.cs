﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screamer : MonoBehaviour
{
    private GameObject trigger;
    [SerializeField]
    private Transform screamerImage;
    [SerializeField]
    private AudioSource audioScream;
    [SerializeField]
    private float timerScreamer;
    void Start()
    {
         //trigger.SetActive(true);
        // timerScreamer = 2;
        screamerImage.gameObject.SetActive(false);
    }

    // void Update()
    // {
    //     if (timerScreamer > 0 && screamerImage.gameObject.activeSelf == true)
    //     {
    //         timerScreamer -= Time.deltaTime;
    //     }
    //     else if(timerScreamer <= 0 && screamerImage.gameObject.activeSelf == true)
    //     {

    //         screamerImage.gameObject.SetActive(false);
    //         audioScream.Stop();
    //         timerScreamer = 2;
    //         trigger.SetActive(false);
    //     }
    // }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            screamerImage.gameObject.SetActive(true);
            audioScream.Play();
            Destroy(trigger, timerScreamer);
            Destroy(screamerImage.gameObject, timerScreamer);
        }
    }
}
