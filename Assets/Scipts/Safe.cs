﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Safe : MonoBehaviour
{

    private bool isOpen;

    private bool IsLocked = true;
    private float safeOpenAngle = 90f;
    private float safeCloseAngle = 0f;
    private float smooth = 2f;

    void Start()
    {

    }


    void Update()
    {
        if (IsLocked) return;
        if (isOpen)
        {
            Quaternion targetRotationOpen = Quaternion.Euler(0, safeOpenAngle, 0);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotationOpen, smooth * Time.deltaTime);
        }
        else
        {
             Quaternion targetRotationClosed = Quaternion.Euler(0, safeOpenAngle, 0);
            // transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotationClosed, smooth * Time.deltaTime);
            transform.rotation=Quaternion.Slerp(transform.rotation, targetRotationClosed, smooth * Time.deltaTime);
        }
    }
     public void Unlock()
    {
        IsLocked = false;
    }
    public void ChangeSafeState()
    {
        isOpen = !isOpen;
    }
}
