﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : MonoBehaviour
{
    [SerializeField]
    private DoorWithNote door;

    private void Start()
    {
        
    }
    public void PickUp()
    {
        var notes = PlayerPrefs.GetInt("Notes");
        notes++;
        PlayerPrefs.SetInt("Notes", notes);
        //Destroy(gameObject);
    }
}
