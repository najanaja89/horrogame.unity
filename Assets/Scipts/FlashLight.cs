﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashLight : MonoBehaviour
{
    [SerializeField]
    private Image batteryIndicator;
    [SerializeField]
    private Light flashLight;
    private bool isOn = true;
    [SerializeField]
    private float maxBatteryLife, lightDrain;
    private float curBatteryLife;
    void Start()
    {
        curBatteryLife = maxBatteryLife;
    }

    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.F))
        {
            isOn = !isOn;
            flashLight.enabled = isOn;
        }
        if (isOn)
        {
            if (curBatteryLife > 0)
            {
                curBatteryLife -= lightDrain * Time.deltaTime;
                flashLight.intensity=curBatteryLife;
                var batterySize= new Vector3(curBatteryLife/maxBatteryLife, batteryIndicator.transform.lossyScale.y,batteryIndicator.transform.lossyScale.z);
                batteryIndicator.transform.localScale=batterySize;
            }
            else
            {
                curBatteryLife=0;
                isOn=false;
                flashLight.enabled=false;
            }
        }
    }
    public void FlashLightEnergyAdd(float capacity)
    {
        curBatteryLife=capacity;
    }

      public void FlashLightEnergyMinus(float capacity)
    {
        curBatteryLife-=capacity;
    }
}
